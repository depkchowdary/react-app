import React, { Component } from 'react';

//components import

import Toolbar from './components/toolbar/toolbar'
import SideDrawer from './components/SideDrawer/SideDrawer'
import BackDrop from './components/BackDrop/BackDrop'
import StickySidebar from './components/StickySideBar/StickySidebar'

import AppContainer from './components/appContainer/AppContainer'
import {BrowserRouter} from 'react-router-dom'

import './App.css'

class App extends Component {
  state = {
    sideDrawerOpen : false
  }
  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen : !prevState.sideDrawerOpen}
    });
  };

  backDropClickHandler = () => {
    this.setState({sideDrawerOpen: false})
  }

  render() {
    let backDrop; 

    if(this.state.sideDrawerOpen){
      backDrop = <BackDrop click={this.backDropClickHandler}/>
    }

    return (
      <BrowserRouter>
        <div style={{height: '100vh'}}>
          <Toolbar drawerClickHandler = {this.drawerToggleClickHandler}/>
          <SideDrawer show={this.state.sideDrawerOpen}/>
          {backDrop}
          <div className="staging-area"> 
            <StickySidebar/>
            <AppContainer/> 
          </div>
        </div> 
      </BrowserRouter>
    );
  }
}

export default App;

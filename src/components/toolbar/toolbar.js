import React from 'react'

import './toolbar.css'
import DrawerToggleButton from '../SideDrawer/DrawerToggleButton'

import {Link} from 'react-router-dom'

const toolbar = (props) => (
    <header className="toolbar">
        <nav className="toolbar__navigation">
            <div className="toolbar__toggle-button">
                <DrawerToggleButton click={props.drawerClickHandler}></DrawerToggleButton>
            </div>
            <div className="toolbar__logo"><Link to="/"><i className="fab fa-readme"></i> &nbsp;readMe</Link></div>
            <div className="divider"></div>
            <div className="toolbar_navigation-items">
                <ul>
                    <li><Link  to="/register">Sign up!</Link></li>
                    <li><Link to="/login">Login</Link></li>
                </ul>
            </div>
        </nav>
    </header>
);

export default toolbar
import React from 'react';

import './SideDrawer.css'

const SideBar = (props) => {
    let drawerClasses = 'sidebar'
    if(props.show){
        drawerClasses = 'sidebar open'
    }
    return (<nav className={drawerClasses}>
        <ul>
            <li><a href="/register">Sign up!</a></li>
            <li><a href="/Login">Login</a></li>
        </ul>
    </nav>
    );
}

export default SideBar;
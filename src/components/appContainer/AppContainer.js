import React, {Component} from 'react';

import SnapshotList from '../SnapshotList/SnapshotList';
import LandingPage from '../LandingPage/LandingPage'

import './AppContainer.css'

import {Route} from 'react-router-dom'

class AppContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            feeds : [
            {
                feedName : 'NASA',
                feedUrl: ''
            },
            {
                feedName : 'Times of India',
                feedUrl: ''
            },
            {
                feedName : 'Economic Times',
                feedUrl: ''
            }
        ],
        favs : [
            {
                feedName : 'NASA',
                feedUrl: ''
            }
        ]
        }
    }
    
    render(){
        return (
            <div className="app-container">
                <Route exact path="/" render={() => <LandingPage/>} />
                <Route path="/feeds" render={() => <SnapshotList list={this.state.feeds}/>} />
                <Route path="/favs" render={() => <SnapshotList list={this.state.favs} />} />
            </div>
        );
    }
}

export default AppContainer;
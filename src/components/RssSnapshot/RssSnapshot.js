import React from 'react';

import './RssSnapshot.css'

class RssSnapshot extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            feedName: props.feedName,
            feedUrl: ""
        }
    }
    render() {
        return (
            <div className="snapshot-card">
                <div className="snapshot-logo" style={{width: '75%', height: '75%'}}>Logo</div>
                <div className="snapshot-card-details">
                    <strong> {this.state.feedName} </strong>
                </div>
            </div>
        );
    }
}


export default RssSnapshot;

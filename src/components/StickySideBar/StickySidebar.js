import React from 'react';
import {Link} from 'react-router-dom'

import './StickySidebar.css'

//import Favourites from '../Favourites/Favourites'


const StickySidebar = props => {
    return (
        <div className="sticky-sidebar">
            <ul className="sticky-sidebar-items">
                {/* <Router>
                    <Route path="/favs" component={Favourites}><i className="fas fa-heart"></i></Route>
                    <Route href="/feeds" component={SnapshotList}><i className="fas fa-rss-square"></i></Route>
                </Router> */}
                <li><Link to="/favs"><i className="fas fa-heart"></i></Link></li>
                <li><Link to="/feeds"><i className="fas fa-rss-square"></i></Link></li>
            </ul>
        </div>
    );
}

export default StickySidebar
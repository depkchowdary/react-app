import React from 'react';


const LandingPage = props => (
    <div className="welcome-card">
        <h1>Hello {props.username || "Stranger"}, How are ya?</h1>
        <hr/>
        <h3>Welcome to readMe! An app to add all your RSS feeds to and read on the Go! On Mobile, On Desktop.</h3>
    </div>
);

export default LandingPage
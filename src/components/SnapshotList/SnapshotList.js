import React from 'react';

import RssSnapshot from '../RssSnapshot/RssSnapshot';

const SnapshotList = props => {
    return (
        props.list.map((feed) => {
            return (
                <RssSnapshot feedName={feed.feedName} key={feed.feedName} />
            )
        })
    )
}


export default SnapshotList;